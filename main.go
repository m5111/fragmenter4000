package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/h2non/filetype"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"

	"github.com/therecipe/qt/widgets"
)

type Movie struct {
	MovieName string
	Extension string
	Path      string
	Lines     []Line
}

type Line struct {
	Number       int
	TimeString   string
	StartTime    time.Time
	StopTime     time.Time
	LinesStrings []string
}

func (line *Line) timestampParser() error {
	match, err := regexp.MatchString("[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3} --> [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}", line.TimeString)
	if !match || err != nil {
		return err
	}
	tempString := strings.Split(line.TimeString, " --> ")
	startTimeString := strings.Replace(tempString[0], ",", ".", 1)
	stopTimeString := strings.Replace(tempString[1], ",", ".", 1)

	line.StartTime, err = time.Parse("15:04:05.999", startTimeString)
	if err != nil {
		return err
	}
	line.StopTime, err = time.Parse("15:04:05.999", stopTimeString)
	if err != nil {
		return err
	}
	return nil
}

func findMovie(fileNames *[]string, movieName string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.Contains(info.Name(), movieName) {
			if !info.IsDir() {
				f, err := os.Open(path)
				if err != nil {
					return err
				}
				defer f.Close()
				head := make([]byte, 261)
				f.Read(head)
				if filetype.IsVideo(head) {
					*fileNames = append(*fileNames, path)
				}
			}
		}
		return nil
	}
}

func movieFileNameFinder(path string, movieName string) (string, error) {
	var files []string
	err := filepath.Walk(path, findMovie(&files, movieName))
	if err != nil {
		return "", err
	}
	if len(files) > 0 {
		return files[0], nil
	}
	return "", errors.New("Nie znaleziono filmu w \"" + path + "\"!")
}

type LinesModel struct {
	core.QAbstractListModel
	_         func()          `constructor:"init"`
	_         func(item Line) `signal:"add,auto"`
	_         func()          `signal:"clear,auto"`
	modelData []Line
}

func (m *LinesModel) init() {
	m.modelData = []Line{}
	m.ConnectRowCount(m.rowCount)
	m.ConnectData(m.data)
}

func (m *LinesModel) rowCount(*core.QModelIndex) int {
	return len(m.modelData)
}

func (m *LinesModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if role != int(core.Qt__DisplayRole) {
		return core.NewQVariant()
	}

	item := m.modelData[index.Row()]
	return core.NewQVariant14(item.TimeString)
}

func (m *LinesModel) add(item Line) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, item)
	m.EndInsertRows()
}

func (m *LinesModel) clear() {
	m.BeginRemoveRows(core.NewQModelIndex(), 0, len(m.modelData)-1)
	m.modelData = []Line{}
	m.EndRemoveRows()
}

type MoviesModel struct {
	core.QAbstractListModel
	_         func()           `constructor:"init"`
	_         func(item Movie) `signal:"add,auto"`
	_         func()           `signal:"clear,auto"`
	modelData []Movie
}

func (m *MoviesModel) init() {
	m.modelData = []Movie{}
	m.ConnectRowCount(m.rowCount)
	m.ConnectData(m.data)
}

func (m *MoviesModel) add(item Movie) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, item)
	m.EndInsertRows()
}

func (m *MoviesModel) clear() {
	m.BeginRemoveRows(core.NewQModelIndex(), 0, len(m.modelData)-1)
	m.modelData = []Movie{}
	m.EndRemoveRows()
}

func (m *MoviesModel) rowCount(*core.QModelIndex) int {
	return len(m.modelData)
}

func (m *MoviesModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if role != int(core.Qt__DisplayRole) {
		return core.NewQVariant()
	}

	item := m.modelData[index.Row()]
	return core.NewQVariant14(item.MovieName)
}

type LibraryModel struct {
	core.QAbstractListModel
	_         func()                        `constructor:"init"`
	_         func(item string)             `signal:"add,auto"`
	_         func(index *core.QModelIndex) `signal:"remove,auto"`
	modelData []string
}

func (m *LibraryModel) init() {
	m.modelData = []string{}
	m.ConnectRowCount(m.rowCount)
	m.ConnectData(m.data)
	m.loadLibrary()
}

func (m *LibraryModel) add(item string) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, item)
	m.EndInsertRows()
	err := m.saveLibrary()
	if err != nil {
		widgets.QMessageBox_Critical(nil, "ERROR", err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	}
}

func (m *LibraryModel) remove(index *core.QModelIndex) {
	m.BeginRemoveRows(core.NewQModelIndex(), index.Row(), index.Row())
	m.modelData = append(m.modelData[:index.Row()], m.modelData[index.Row()+1:]...)
	m.EndRemoveRows()
	err := m.saveLibrary()
	if err != nil {
		widgets.QMessageBox_Critical(nil, "ERROR", err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	}
}

func (m *LibraryModel) rowCount(*core.QModelIndex) int {
	return len(m.modelData)
}

func (m *LibraryModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if role != int(core.Qt__DisplayRole) {
		return core.NewQVariant()
	}

	item := m.modelData[index.Row()]
	return core.NewQVariant14(item)
}

func (m *LibraryModel) loadLibrary() error {
	f, err := os.OpenFile("library.txt", os.O_CREATE|os.O_RDONLY, 0600)
	if err != nil {
		return err
	}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
		m.modelData = append(m.modelData, scanner.Text())
		m.EndInsertRows()
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

func (m *LibraryModel) saveLibrary() error {
	f, err := os.OpenFile("library.txt", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	for _, key := range m.modelData {
		_, err := f.WriteString(key + "\n")
		if err != nil {
			return err
		}
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

func subtitlesFilesList(list *[]string) filepath.WalkFunc {
	return func(path string, f os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".srt") {
			*list = append(*list, path)
		}
		return nil
	}
}

func findSubtitlesFiles(path string) []string {
	var foundSrtFiles []string
	err := filepath.Walk(path, subtitlesFilesList(&foundSrtFiles))
	if err != nil {
		//walk.MsgBox(nil,"Błąd!",err.Error(),walk.MsgBoxIconError)
	}
	return foundSrtFiles
}

func parseSTR(file *os.File, model *MoviesModel, movie Movie, fraze string) {
	scanner := bufio.NewScanner(file)
	var err error
	line := new(Line)
	i := 0
	for scanner.Scan() {
		if strings.Compare(scanner.Text(), "") == 0 {
			for _, lineIterator := range line.LinesStrings {
				if strings.Contains(strings.ToUpper(lineIterator), strings.ToUpper(fraze)) {
					movie.Lines = append(movie.Lines, *line)
				}
			}
			line = new(Line)
			i = 0
		} else {
			if i == 0 {
				line.Number, err = strconv.Atoi(scanner.Text())
				if err != nil {
					continue
				}
				i++
			} else if i == 1 {
				line.TimeString = scanner.Text()
				i++
			} else {
				line.LinesStrings = append(line.LinesStrings, scanner.Text())
			}
		}
	}
	if len(movie.Lines) > 0 {
		model.Add(movie)
	}
}

func searchLibraryElement(libraryElement string, model *MoviesModel, fraze string) error {
	for _, filePath := range findSubtitlesFiles(libraryElement) {
		file, err := os.Open(filePath)
		if err != nil {
			return err
		}
		var movie Movie
		movie.Path = filepath.Dir(filePath)
		movie.MovieName = strings.Split(filepath.Base(filePath), ".srt")[0]
		parseSTR(file, model, movie, fraze)
	}
	return nil
}

func findFraze(fraze string, model *MoviesModel, library []string) {
	model.Clear()
	for _, libraryElement := range library {
		go searchLibraryElement(libraryElement, model, fraze)
	}
}

func (movie *Movie) prepareForPlayback(line *Line) error {
	movieFileName, err := movieFileNameFinder(movie.Path, movie.MovieName)
	if err != nil {
		return err
	}
	movie.Extension = movieFileName[strings.LastIndexByte(movieFileName, '.'):]
	err = line.timestampParser()
	if err != nil {
		return err
	}
	return nil
}

func (movie *Movie) createTempSubtitles(line Line) {
	f, err := os.OpenFile("TEMP.srt", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		//runErrorDialogWithLog(nil, "ERROR", err)
	}
	defer f.Close()
	fmt.Fprintln(f, "1")
	fmt.Fprintln(f, "00:00:00,000 --> 00:02:00,000")
	for _, line := range line.LinesStrings {
		fmt.Fprintln(f, line)
	}
}

func copySubtitles(path string) {
	file, err := os.Open(path)
	if err != nil {
		println(err.Error())
	}
	defer file.Close()
	file2, err := os.Create("TEMP.srt")
	if err != nil {
		println(err.Error())
	}
	defer file2.Close()
	io.Copy(file2, file)
}

type MyMainWindow struct {
	*widgets.QMainWindow
	frazeInput              *widgets.QLineEdit
	searchButton            *widgets.QPushButton
	moviesListView          *widgets.QListView
	moviesModel             *MoviesModel
	linesListView           *widgets.QListView
	linesModel              *LinesModel
	libraryListView         *widgets.QListView
	libraryModel            *LibraryModel
	textBrowser             *widgets.QTextEdit
	addToLibraryButton      *widgets.QPushButton
	removeFromLibraryButton *widgets.QPushButton
	libraryLineEdit         *widgets.QLineEdit
	startOffsetNumberEdit   *widgets.QDoubleSpinBox
	stopOffsetNumberEdit    *widgets.QDoubleSpinBox
	saveButton              *widgets.QPushButton
	playButton              *widgets.QPushButton
	movieImageView          *widgets.QLabel
	firstColumnWidget       *widgets.QWidget
	secondColumnWidget      *widgets.QWidget
	thirdColumnWidget       *widgets.QWidget
	fourthColumnWidget      *widgets.QWidget
	centralWidget           *widgets.QWidget
}

func (mw *MyMainWindow) addToLibrary(bool) {
	dir, err := os.Open(mw.libraryLineEdit.Text())
	if err != nil {
		//return errors.New("Nie ma takiego katalogu!")
		return
	}
	fi, err := dir.Stat()
	if err != nil {
		//return errors.New("Błąd odczytu katalogu!")
		return
	}
	if !fi.IsDir() {
		//return errors.New("Plik nie jest katalogiem!")
		return
	}
	dir.Close()
	for _, element := range mw.libraryModel.modelData {
		if strings.Compare(element, mw.libraryLineEdit.Text()) == 0 {
			//return errors.New("Katalog jest już na liście!")
			return
		}
	}
	mw.libraryModel.Add(mw.libraryLineEdit.Text())
	mw.libraryLineEdit.SetText("")
}

func (mw *MyMainWindow) removeFromLibrary(bool) {
	mw.libraryModel.Remove(mw.libraryListView.CurrentIndex())
}

func (mw *MyMainWindow) search(bool) {
	go findFraze(mw.frazeInput.Text(), mw.moviesModel, mw.libraryModel.modelData)
	//mw.statusLabel.SetText("Znaleziono " + strconv.Itoa(len(mw.moviesListBox.Model().(*MoviesModel).foundMovies)) + " wyników.")
}

func (mw *MyMainWindow) librarySelected(current *core.QModelIndex, previous *core.QModelIndex) {
}

func (mw *MyMainWindow) movieSelected(current *core.QModelIndex, previous *core.QModelIndex) {
	mw.linesModel.Clear()
	if current.Row() >= 0 {
		for _, line := range mw.moviesModel.modelData[current.Row()].Lines {
			mw.linesModel.Add(line)
		}
		abstractItemModel := core.NewQAbstractItemModel(nil)
		abstractItemModel.Index(0, 0, mw.libraryListView.CurrentIndex())
	}
}

func (mw *MyMainWindow) lineSelected(current *core.QModelIndex, previous *core.QModelIndex) {
	mw.textBrowser.SetText("")
	if current.Row() >= 0 {
		mw.snapshot()
		for _, line := range mw.linesModel.modelData[current.Row()].LinesStrings {
			mw.textBrowser.Append(line)
		}
	}
}

func (mw *MyMainWindow) snapshot() {
	if mw.moviesListView.CurrentIndex().Row() < 0 || mw.linesListView.CurrentIndex().Row() < 0 {
		return
	}
	movie := mw.moviesModel.modelData[mw.moviesListView.CurrentIndex().Row()]
	line := movie.Lines[mw.linesListView.CurrentIndex().Row()]
	err := movie.prepareForPlayback(&line)
	if err != nil {
		widgets.QMessageBox_Critical(mw, "ERROR", err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
		return
	}
	movie.createTempSubtitles(line)
	executableSuffix := ""
	if runtime.GOOS == "windows" {
		executableSuffix = ".exe"
	}
	cmd := exec.Command(
		"ffmpeg"+executableSuffix,
		"-y",
		"-ss", line.StartTime.Format("15:04:05.999"),
		"-i", movie.Path+string(os.PathSeparator)+movie.MovieName+movie.Extension,
		"-vf",
		"scale="+strconv.Itoa(mw.fourthColumnWidget.Size().Width()-18)+":-1, subtitles=TEMP.srt",
		"-frames:v", "1",
		"scene.jpg")
	output, err := cmd.CombinedOutput()
	if err != nil {
		widgets.QMessageBox_Critical(mw, "ERROR", err.Error()+string(output), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	}
	fmt.Println(string(output))
	var pic = gui.NewQPixmap5("scene.jpg", "", core.Qt__AutoColor)
	mw.movieImageView.SetPixmap(pic)
	os.Remove("TEMP.srt")
}
func (mw *MyMainWindow) save(bool) {
	if mw.moviesListView.CurrentIndex().Row() < 0 || mw.linesListView.CurrentIndex().Row() < 0 {
		return
	}
	movie := mw.moviesModel.modelData[mw.moviesListView.CurrentIndex().Row()]
	line := movie.Lines[mw.linesListView.CurrentIndex().Row()]
	err := movie.prepareForPlayback(&line)
	if err != nil {
		widgets.QMessageBox_Critical(mw, "ERROR", err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
		return
	}
	var fileDialog = widgets.NewQFileDialog2(mw, "Zapisz jako...", "", "")
	fileDialog.SetAcceptMode(widgets.QFileDialog__AcceptSave)
	fileDialog.SetDefaultSuffix(".mp4")
	if fileDialog.Exec() != int(widgets.QDialog__Accepted) {
		return
	}
	movie.createTempSubtitles(line)
	startTimeOffset := time.Duration(mw.startOffsetNumberEdit.Value()*1000) * time.Millisecond
	stopTimeOffset := time.Duration(mw.stopOffsetNumberEdit.Value()*1000) * time.Millisecond
	press := line.StartTime.Add(startTimeOffset).Add(time.Duration(-60) * time.Second).Format("15:04:05.999")
	duration := line.StopTime.Add(stopTimeOffset).Sub(line.StartTime.Add(startTimeOffset)).Seconds()
	to := fmt.Sprintf("%.3f", duration)
	executableSuffix := ""
	if runtime.GOOS == "windows" {
		executableSuffix = ".exe"
	}
	cmd := exec.Command(
		"ffmpeg"+executableSuffix,
		"-y",
		"-ss", press,
		"-i", movie.Path+string(os.PathSeparator)+movie.MovieName+movie.Extension,
		"-ss", "00:01:00",
		"-t", to,
		"-acodec", "copy",
		"-vcodec", "h264",
		"-preset", "veryslow",
		"-vf", "subtitles=TEMP.srt",
		fileDialog.SelectedFiles()[0])

	progressLabel := widgets.NewQLabel2("Konwertowanie...", nil, 0)
	converterInfo := widgets.NewQDialog(mw, 0)
	progressBar := widgets.NewQProgressBar(nil)
	converterInfo.SetWindowTitle("Konwertowanie...")
	converterInfo.SetLayout(widgets.NewQVBoxLayout())
	converterInfo.Layout().AddWidget(progressLabel)
	converterInfo.Layout().AddWidget(progressBar)
	converterInfo.SetWindowFlags(core.Qt__Window | core.Qt__WindowMinimizeButtonHint)
	converterInfo.Show()
	converterInfo.Raise()

	stdout, _ := cmd.StderrPipe()
	go func() {
		f, err := os.OpenFile("log.txt", os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			//runErrorDialogWithLog(nil, "ERROR", err)
		}
		defer f.Close()

		var current, percent float64

		progressBar.SetRange(0, 99)

		cmd.Start()
		r := bufio.NewScanner(stdout)
		splitFunc := func(data []byte, atEOF bool) (advance int, token []byte, spliterror error) {
			if atEOF && len(data) == 0 {
				return 0, nil, nil
			}
			if i := bytes.IndexByte(data, '\n'); i >= 0 {
				return i + 1, data[0:i], nil
			}
			if i := bytes.IndexByte(data, '\r'); i >= 0 {
				return i + 1, data[0:i], nil
			}
			if atEOF {
				return len(data), data, nil
			}

			return 0, nil, nil
		}
		timeToSec := func(time string) (float64, error) {
			var result float64
			split := strings.Split(time, ":")
			hours, err := strconv.Atoi(split[0])
			minutes, err := strconv.Atoi(split[1])
			seconds, err := strconv.ParseFloat(split[2], 64)
			result = float64(hours*3600) + float64(minutes*60) + seconds
			return result, err
		}
		r.Split(splitFunc)
		for r.Scan() {
			line := r.Text()
			if strings.Contains(line, "frame=") {
				index := strings.Index(line, "time=") + 5
				end := index + 11
				current, _ = timeToSec(line[index:end])
				fmt.Println(current)
				fmt.Println(duration)
				percent = current * 100 / duration
				progressBar.SetValue(int(percent))
			}
		}
		cmd.Wait()
		converterInfo.Accept()
		converterInfo.Close()
		os.Remove("TEMP.srt")
	}()
}

func (mw *MyMainWindow) play(bool) {
	if mw.moviesListView.CurrentIndex().Row() > -1 && mw.linesListView.CurrentIndex().Row() > -1 {
		movie := mw.moviesModel.modelData[mw.moviesListView.CurrentIndex().Row()]
		line := movie.Lines[mw.linesListView.CurrentIndex().Row()]
		err := movie.prepareForPlayback(&line)
		if err != nil {
			widgets.QMessageBox_Critical(mw, "ERROR", err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
			return
		}
		movie.createTempSubtitles(line)
		executableSuffix := ""
		if runtime.GOOS == "windows" {
			executableSuffix = ".exe"
		}
		cmd := exec.Command(
			"ffmpeg"+executableSuffix,
			"-y",
			"-ss", line.StartTime.Add(time.Duration(mw.startOffsetNumberEdit.Value())*time.Second).Format("15:04:05.999"),
			"-to", line.StopTime.Add(time.Duration(mw.stopOffsetNumberEdit.Value())*time.Second).Format("15:04:05.999"),
			"-i", movie.Path+string(os.PathSeparator)+movie.MovieName+movie.Extension,
			"-vf",
			"subtitles=TEMP.srt",
			"temp"+movie.Extension)
		output, err := cmd.CombinedOutput()
		if err != nil {
			os.Stderr.WriteString(err.Error())
			widgets.QMessageBox_Critical(mw, "ERROR", err.Error()+string(output), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
		}
		fmt.Println(string(output))
		os.Remove("TEMP.srt")
		//mw.movieImageView = imageViewer("temp" + movie.Extension)
	}
}

func imageViewer(imageFileName string) *widgets.QWidget {
	var (
		displayArea *widgets.QWidget
		scene       *widgets.QGraphicsScene
		view        *widgets.QGraphicsView
		item        *widgets.QGraphicsPixmapItem
	)

	displayArea = widgets.NewQWidget(nil, 0)
	scene = widgets.NewQGraphicsScene(nil)
	view = widgets.NewQGraphicsView(nil)

	var imageReader = gui.NewQImageReader3(imageFileName, core.NewQByteArray())
	// test to see if we are dealing with animated GIF
	//fmt.Println("Animated GIF : ", imageReader.SupportsAnimation())

	if imageReader.SupportsAnimation() {
		// instead of reading from file(disk) again, we take from memory
		// HOWEVER, this will cause segmentation violation error ! :(
		//var movie = gui.NewQMovieFromPointer(imageReader.Pointer())
		var movie = gui.NewQMovie3(imageFileName, core.NewQByteArray(), nil)

		// see http://stackoverflow.com/questions/5769766/qt-how-to-show-gifanimated-image-in-qgraphicspixmapitem
		var movieLabel = widgets.NewQLabel(nil, core.Qt__Widget)
		movieLabel.SetMovie(movie)
		movie.Start()
		scene.AddWidget(movieLabel, core.Qt__Widget)
	} else {
		widgets.QMessageBox_Critical(nil, "ERROR", "error", widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
		var pixmap = gui.NewQPixmap5(imageFileName, "", core.Qt__AutoColor)
		item = widgets.NewQGraphicsPixmapItem2(pixmap, nil)
		scene.AddItem(item)
	}

	view.SetScene(scene)

	//create a button and connect the clicked signal

	var layout = widgets.NewQVBoxLayout()

	layout.AddWidget(view, 1, core.Qt__AlignCenter)

	displayArea.SetLayout(layout)

	return displayArea
}

func main() {

	// needs to be called once before you can start using the QWidgets
	app := widgets.NewQApplication(len(os.Args), os.Args)

	mainWindow := new(MyMainWindow)

	// create a window
	// with a minimum size of 250*200
	// and sets the title to "Hello Widgets Example"
	mainWindow.QMainWindow = widgets.NewQMainWindow(nil, 0)
	mainWindow.QMainWindow.SetMinimumSize2(1800, 600)
	mainWindow.QMainWindow.SetWindowTitle("Fragmentator 4000")

	// Central Widget
	mainWindow.centralWidget = widgets.NewQWidget(nil, 0)
	mainWindow.centralWidget.SetLayout(widgets.NewQHBoxLayout())
	mainWindow.QMainWindow.SetCentralWidget(mainWindow.centralWidget)

	// First Column

	mainWindow.firstColumnWidget = widgets.NewQWidget(mainWindow.centralWidget, 0)
	mainWindow.firstColumnWidget.SetLayout(widgets.NewQVBoxLayout())
	mainWindow.firstColumnWidget.SetFixedWidth(200)
	mainWindow.centralWidget.Layout().AddWidget(mainWindow.firstColumnWidget)

	frazeLabel := widgets.NewQLabel2("Wpisz frazę:", nil, 0)
	frazeLabel.SetAlignment(core.Qt__AlignTop)
	mainWindow.firstColumnWidget.Layout().AddWidget(frazeLabel)

	mainWindow.frazeInput = widgets.NewQLineEdit(nil)
	mainWindow.frazeInput.SetPlaceholderText("Wpisz frazę ...")
	mainWindow.frazeInput.ConnectReturnPressed(func() {
		mainWindow.search(true)
	})
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.frazeInput)

	libraryLabel := widgets.NewQLabel2("Szukaj w:", nil, 0)
	libraryLabel.SetAlignment(core.Qt__AlignTop)
	mainWindow.firstColumnWidget.Layout().AddWidget(libraryLabel)

	mainWindow.libraryListView = widgets.NewQListView(nil)
	mainWindow.libraryModel = NewLibraryModel(nil)
	mainWindow.libraryListView.SetModel(mainWindow.libraryModel)
	mainWindow.libraryListView.ConnectCurrentChanged(mainWindow.librarySelected)
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.libraryListView)

	mainWindow.libraryLineEdit = widgets.NewQLineEdit(nil)
	mainWindow.libraryLineEdit.SetPlaceholderText("Szukaj w ...")
	mainWindow.libraryLineEdit.ConnectReturnPressed(func() {
		mainWindow.addToLibrary(true)
	})
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.libraryLineEdit)

	mainWindow.addToLibraryButton = widgets.NewQPushButton2("Dodaj", nil)
	mainWindow.addToLibraryButton.ConnectClicked(mainWindow.addToLibrary)
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.addToLibraryButton)

	mainWindow.removeFromLibraryButton = widgets.NewQPushButton2("Usuń", nil)
	mainWindow.removeFromLibraryButton.ConnectClicked(mainWindow.removeFromLibrary)
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.removeFromLibraryButton)

	startOffsetLabel := widgets.NewQLabel2("Przesunięcie startu", nil, 0)
	mainWindow.firstColumnWidget.Layout().AddWidget(startOffsetLabel)

	mainWindow.startOffsetNumberEdit = widgets.NewQDoubleSpinBox(nil)
	mainWindow.startOffsetNumberEdit.SetSuffix("s")
	mainWindow.startOffsetNumberEdit.SetSingleStep(0.1)
	mainWindow.startOffsetNumberEdit.SetMinimum(-59.99)
	mainWindow.startOffsetNumberEdit.SetMaximum(59.99)
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.startOffsetNumberEdit)

	stopOffsetLabel := widgets.NewQLabel2("Przesunięcie Końca", nil, 0)
	mainWindow.firstColumnWidget.Layout().AddWidget(stopOffsetLabel)

	mainWindow.stopOffsetNumberEdit = widgets.NewQDoubleSpinBox(nil)
	mainWindow.stopOffsetNumberEdit.SetSuffix("s")
	mainWindow.stopOffsetNumberEdit.SetSingleStep(0.1)
	mainWindow.stopOffsetNumberEdit.SetMinimum(-59.99)
	mainWindow.stopOffsetNumberEdit.SetMaximum(59.99)
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.stopOffsetNumberEdit)

	mainWindow.saveButton = widgets.NewQPushButton2("Zapisz", nil)
	mainWindow.saveButton.ConnectClicked(mainWindow.save)
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.saveButton)

	mainWindow.searchButton = widgets.NewQPushButton2("Szukaj", nil)
	mainWindow.searchButton.ConnectClicked(mainWindow.search)
	mainWindow.firstColumnWidget.Layout().AddWidget(mainWindow.searchButton)

	// Second Column

	mainWindow.secondColumnWidget = widgets.NewQWidget(mainWindow.centralWidget, 0)
	mainWindow.secondColumnWidget.SetLayout(widgets.NewQVBoxLayout())
	mainWindow.centralWidget.Layout().AddWidget(mainWindow.secondColumnWidget)

	filmsLabel := widgets.NewQLabel2("Filmy:", nil, 0)
	filmsLabel.SetAlignment(core.Qt__AlignCenter)
	mainWindow.secondColumnWidget.Layout().AddWidget(filmsLabel)

	mainWindow.moviesListView = widgets.NewQListView(nil)
	mainWindow.moviesModel = NewMoviesModel(nil)
	mainWindow.moviesListView.SetModel(mainWindow.moviesModel)
	mainWindow.moviesListView.ConnectCurrentChanged(mainWindow.movieSelected)
	mainWindow.secondColumnWidget.Layout().AddWidget(mainWindow.moviesListView)

	// Third Column

	mainWindow.thirdColumnWidget = widgets.NewQWidget(mainWindow.centralWidget, 0)
	mainWindow.thirdColumnWidget.SetLayout(widgets.NewQVBoxLayout())
	mainWindow.thirdColumnWidget.SetFixedWidth(210)
	mainWindow.centralWidget.Layout().AddWidget(mainWindow.thirdColumnWidget)

	linesLabel := widgets.NewQLabel2("Linie:", nil, 0)
	linesLabel.SetAlignment(core.Qt__AlignCenter)
	mainWindow.thirdColumnWidget.Layout().AddWidget(linesLabel)

	mainWindow.linesListView = widgets.NewQListView(nil)
	mainWindow.linesModel = NewLinesModel(nil)
	mainWindow.linesListView.SetModel(mainWindow.linesModel)
	mainWindow.linesListView.ConnectCurrentChanged(mainWindow.lineSelected)
	mainWindow.thirdColumnWidget.Layout().AddWidget(mainWindow.linesListView)

	// Fourth Column

	mainWindow.fourthColumnWidget = widgets.NewQWidget(mainWindow.centralWidget, 0)
	mainWindow.fourthColumnWidget.SetLayout(widgets.NewQVBoxLayout())
	mainWindow.centralWidget.Layout().AddWidget(mainWindow.fourthColumnWidget)

	mainWindow.movieImageView = widgets.NewQLabel(nil, core.Qt__Widget)
	sizePolicy := widgets.NewQSizePolicy()
	mainWindow.movieImageView.SetScaledContents(true)
	sizePolicy.SetHorizontalPolicy(widgets.QSizePolicy__Expanding)
	sizePolicy.SetVerticalPolicy(widgets.QSizePolicy__Expanding)
	mainWindow.movieImageView.SetSizePolicy(sizePolicy)

	mainWindow.fourthColumnWidget.Layout().AddWidget(mainWindow.movieImageView)

	mainWindow.playButton = widgets.NewQPushButton2("Odtwórz", nil)
	mainWindow.playButton.ConnectClicked(mainWindow.play)
	mainWindow.fourthColumnWidget.Layout().AddWidget(mainWindow.playButton)

	mainWindow.textBrowser = widgets.NewQTextEdit(nil)
	mainWindow.fourthColumnWidget.Layout().AddWidget(mainWindow.textBrowser)

	mainWindow.QMainWindow.Show()

	app.Exec()
}
